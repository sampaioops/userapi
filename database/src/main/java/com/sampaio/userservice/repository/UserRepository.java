package com.sampaio.userservice.repository;

import com.sampaio.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByDocumentAndPassword(String document, String password);

    @Query("SELECT u FROM User u WHERE u.name like %:name%")
    List<User> queryByNameLike(String name);

}
