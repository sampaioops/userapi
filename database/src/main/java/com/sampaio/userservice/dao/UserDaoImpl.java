package com.sampaio.userservice.dao;

import com.sampaio.userservice.model.User;
import com.sampaio.userservice.repository.UserDao;
import com.sampaio.userservice.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserDaoImpl implements UserDao {

    private final UserRepository userRepository;

    public UserDaoImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> findByDocumentAndPassword(final String document, final String password) {
        return userRepository.findByDocumentAndPassword(document, password);
    }

    @Override
    public List<User> queryByNameLike(final String name) {
        return userRepository.queryByNameLike(name);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }
}
