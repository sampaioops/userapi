package com.sampaio.userservice;

import com.sampaio.userservice.model.User;
import com.sampaio.userservice.repository.UserDao;
import com.sampaio.userservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;

@ActiveProfiles("test")
@ContextConfiguration(classes = TestContextConfiguration.class)
@SpringBootTest
public abstract class AbstractTest {

    @Autowired
    protected UserDao userDao;

    @Autowired
    protected UserRepository userRepository;


    protected User buildUser() {
        return buildUser("Daniel Sampaio");
    }

    protected User buildUser(final String name) {
        return buildUser(name, "31515050", "asassa");
    }

    protected User buildUser(final String document, final String password) {
        return buildUser("Daniel Sampaio", document, password);
    }

    private User buildUser(final String name,
                           final String document,
                           final String password) {
        return User.builder()
                .name(name)
                .email("sampaioops@gmail.com")
                .document(document)
                .password(password)
                .telephone("22988551")
                .dateCreated(LocalDate.now())
                .build();
    }
}
