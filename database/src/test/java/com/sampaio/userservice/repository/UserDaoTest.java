package com.sampaio.userservice.repository;

import com.sampaio.userservice.AbstractTest;
import com.sampaio.userservice.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserDaoTest extends AbstractTest {

    @Test
    void shouldSavedUser() {
        final var user = buildUser();

        final var save = userDao.save(user);

        Assertions.assertNotNull(save);
    }

    @Test
    void shouldUpdateUser() {
        final var user = buildUser();

        final var userSaved = userDao.save(user);

        final var newEmail = "test@test.com";
        final var userForUpdate = updateEmailUser(userSaved, newEmail);

        assertEquals(newEmail, userForUpdate.getEmail());
    }

    @Test
    void shouldFindById() {
        final var build = buildUser();

        final var userSaved = userDao.save(build);

        final var byId = userDao.findById(userSaved.getId());

        Assertions.assertTrue(byId.isPresent());

        final var userFound = byId.get();

        assertEquals(build.getName(), userFound.getName());
        assertEquals(build.getEmail(), userFound.getEmail());
        assertEquals(build.getPassword(), userFound.getPassword());
        assertEquals(build.getDocument(), userFound.getDocument());
        assertEquals(build.getAddress(), userFound.getAddress());
        assertEquals(build.getTelephone(), userFound.getTelephone());
        assertEquals(build.getDateCreated(), userFound.getDateCreated());
    }

    @Test
    void shouldDeleteUser() {
        final var user = buildUser();

        final var userSaved = userDao.save(user);

        userDao.delete(userSaved);

        final var userItsFound = userDao.findById(userSaved.getId());

        assertTrue(userItsFound.isEmpty());
    }

    @Test
    void shouldFindByDocumentAndPassword(){
        final var document = "174498418";
        final var password = "opt12!";


        final var user = buildUser(document, password);
        userDao.save(user);

        final var found = userDao.findByDocumentAndPassword(document, password);

        assertTrue(found.isPresent());

        final var userFound = found.get();

        assertEquals(document, userFound.getDocument());
        assertEquals(password, userFound.getPassword());
    }

    @Test
    void shouldFindAllUsers(){
        final var user = buildUser();
        final var userSaved = userDao.save(user);

        final var all = userDao.findAll();

        assertFalse(all.isEmpty());

        final var searchUser = all.stream().filter(u -> u.equals(userSaved)).findAny();

        assertTrue(searchUser.isPresent());
    }

    @Test
    void shouldQueryByNameLike(){
        final var name = "Joelma";
        final var user = buildUser(name);
        userDao.save(user);

        final var userByNameList = userDao.queryByNameLike(name);

        assertFalse(userByNameList.isEmpty());
        assertEquals(name, userByNameList.get(0).getName());
    }


    private User updateEmailUser(final User user, final String email) {
        return User.builder()
                .id(user.getId())
                .name(user.getName())
                .email(email)
                .document(user.getDocument())
                .password(user.getPassword())
                .telephone(user.getTelephone())
                .dateCreated(user.getDateCreated())
                .build();
    }
}
