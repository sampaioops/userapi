package com.sampaio.userservice.repository;

import com.sampaio.userservice.AbstractTest;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


class UserRepositoryTest extends AbstractTest {

    @Test
    void shouldSaveUserAndReturnUser() {
        final var user = buildUser();

        final var userSaved = userRepository.save(user);

        assertNotNull(userSaved);
        assertEquals(user.getName(), userSaved.getName());
        assertEquals(user.getDocument(), userSaved.getDocument());
        assertEquals(user.getAddress(), userSaved.getAddress());
        assertEquals(user.getEmail(), userSaved.getEmail());
        assertEquals(user.getPassword(), userSaved.getPassword());
        assertEquals(user.getTelephone(), userSaved.getTelephone());
        assertEquals(user.getDateCreated(), userSaved.getDateCreated());
    }

    @Test
    void shouldfindByIdForUserAndReturnUser() {
        final var user = buildUser();
        final var userSaved = userRepository.save(user);

        final var userFound = userRepository.findById(userSaved.getId());

        assertTrue(userFound.isPresent());
    }

    @Test
    void shouldFindAllUserAndReturnAllUsers() {
        final var user = buildUser();
        final var userSaved = userRepository.save(user);

        final var all = userRepository.findAll();

        assertFalse(all.isEmpty());

        final var searchUser = all.stream().filter(u -> u.equals(userSaved)).findAny();

        assertTrue(searchUser.isPresent());
    }

    @Test
    void shouldDeleteUserAndReturnEmpty() {
        //when
        final var user = buildUser();
        final var userSaved = userRepository.save(user);

        //given
        userRepository.delete(userSaved);

        //then
        final var userFindbyId = userRepository.findById(userSaved.getId());

        assertTrue(userFindbyId.isEmpty());
    }

    @Test
    void shouldFindByDocumentAndPasswordAndReturnUser() {
        final var document = "811117775";
        final var password = "1214!";


        final var user = buildUser(document, password);
        userRepository.save(user);

        final var found = userRepository.findByDocumentAndPassword(document, password);

        assertTrue(found.isPresent());

        final var userFound = found.get();

        assertEquals(document, userFound.getDocument());
        assertEquals(password, userFound.getPassword());
    }

    @Test
    void shouldQueryByNameAndReturnListOfUser() {
        final var name = "Joelma";
        final var user = buildUser(name);
        userRepository.save(user);

        final var userByNameList = userDao.queryByNameLike(name);

        assertFalse(userByNameList.isEmpty());
        assertEquals(name, userByNameList.get(0).getName());
    }
}
