package com.sampaio.userservice.dto;


import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Builder
@Getter
public class UserDTO {

    private final Long id;
    @NotBlank(message = "Name don't blank")
    private final String name;
    @NotBlank(message = "document don't blank")
    private final String document;
    private final String address;
    @NotBlank(message = "Email don't blank")
    private final String email;
    @NotBlank(message = "Password don't blank")
    private final String password;
    private final String telephone;
    private final LocalDate dateCreated;

}
