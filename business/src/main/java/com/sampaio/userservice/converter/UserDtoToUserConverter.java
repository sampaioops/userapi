package com.sampaio.userservice.converter;

import com.sampaio.userservice.dto.UserDTO;
import com.sampaio.userservice.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class UserDtoToUserConverter implements Converter<UserDTO, User> {
    @Override
    public User convert(final UserDTO userDTO) {
        return User.builder()
                .id(userDTO.getId())
                .name(userDTO.getName())
                .document(userDTO.getDocument())
                .address(userDTO.getAddress())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .telephone(userDTO.getTelephone())
                .dateCreated(userDTO.getDateCreated())
                .build();
    }
}
