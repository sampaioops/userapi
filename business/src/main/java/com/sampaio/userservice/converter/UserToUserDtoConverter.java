package com.sampaio.userservice.converter;

import com.sampaio.userservice.dto.UserDTO;
import com.sampaio.userservice.model.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserDtoConverter implements Converter<User, UserDTO> {
    @Override
    public UserDTO convert(final User user) {
        return UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .document(user.getDocument())
                .address(user.getAddress())
                .email(user.getEmail())
                .password(user.getPassword())
                .telephone(user.getTelephone())
                .dateCreated(user.getDateCreated())
                .build();
    }
}
