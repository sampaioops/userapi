package com.sampaio.userservice.service;

import com.sampaio.userservice.dto.UserDTO;

import java.util.List;


public interface UserService {

    List<UserDTO> getAll();

    UserDTO findById(Long id);

    UserDTO save(UserDTO userDTO);

    void delete(Long id);

    UserDTO findByDocumentAndPassword(String document, String password);

    List<UserDTO> queryByName(String name);
}
