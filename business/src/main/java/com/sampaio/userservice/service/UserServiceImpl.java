package com.sampaio.userservice.service;

import com.sampaio.userservice.repository.UserDao;
import com.sampaio.userservice.dto.UserDTO;
import com.sampaio.userservice.exception.UserNotFoundException;
import com.sampaio.userservice.model.User;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    private final ConversionService conversionService;

    public UserServiceImpl(final UserDao userDao, final ConversionService conversionService) {
        this.userDao = userDao;
        this.conversionService = conversionService;
    }

    @Override
    public List<UserDTO> getAll() {
        return userDao.findAll()
                .stream()
                .map(user -> conversionService.convert(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO findById(final Long id) {
        return userDao.findById(id)
                .map(user -> conversionService.convert(user, UserDTO.class))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserDTO save(final UserDTO userDTO) {
        final var save = userDao.save(Objects.requireNonNull(conversionService.convert(userDTO, User.class)));
        return conversionService.convert(save, UserDTO.class);
    }

    @Override
    public void delete(final Long id) {
        userDao.findById(id).ifPresent(userDao::delete);
    }

    @Override
    public UserDTO findByDocumentAndPassword(final String document, final String password) {
        return userDao.findByDocumentAndPassword(document, password)
                .map(user -> conversionService.convert(user, UserDTO.class))
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<UserDTO> queryByName(final String name) {
        return userDao.queryByNameLike(name)
                .stream()
                .map(user -> conversionService.convert(user, UserDTO.class))
                .collect(Collectors.toList());
    }
}
