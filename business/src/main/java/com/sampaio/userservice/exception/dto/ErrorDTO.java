package com.sampaio.userservice.exception.dto;

import lombok.Builder;

import java.util.Date;

@Builder
public class ErrorDTO {
    private final int status;
    private final String message;
    private final Date timestamp;
}
