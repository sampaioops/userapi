package com.sampaio.userservice.repository;

import com.sampaio.userservice.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {

    Optional<User> findByDocumentAndPassword(String document, String password);

    List<User> queryByNameLike(String name);

    List<User> findAll();

    Optional<User> findById(Long id);

    User save(User user);

    void delete(User user);
}
