package com.sampaio.userservice.service;

import com.sampaio.userservice.AbstractTest;
import com.sampaio.userservice.dto.UserDTO;
import com.sampaio.userservice.exception.UserNotFoundException;
import com.sampaio.userservice.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class UserServiceTest extends AbstractTest {

    @Autowired
    private ConversionService conversionService;


    @Test
    void shouldGetAllUsers() {
        when(userDao.findAll())
                .thenReturn(List.of(buildUser(), buildUser()));

        final var all = userService.getAll();

        assertFalse(all.isEmpty());
    }

    @Test
    void shouldSaveUser() {
        final var userBeSaved = buildUser();

        final var userDto = UserDTO.builder()
                .id(userBeSaved.getId())
                .name(userBeSaved.getName())
                .document(userBeSaved.getDocument())
                .email(userBeSaved.getEmail())
                .password(userBeSaved.getPassword())
                .address(userBeSaved.getAddress())
                .telephone(userBeSaved.getTelephone())
                .dateCreated(userBeSaved.getDateCreated())
                .build();

        when(userDao.save(any())).thenReturn(userBeSaved);

        final var userSaved = userService.save(userDto);

        assertNotNull(userSaved.getId());
        assertEquals(userDto.getName(), userSaved.getName());
        assertEquals(userDto.getDocument(), userSaved.getDocument());
        assertEquals(userDto.getEmail(), userSaved.getEmail());
        assertEquals(userDto.getPassword(), userSaved.getPassword());
        assertEquals(userDto.getAddress(), userSaved.getAddress());
        assertEquals(userDto.getTelephone(), userSaved.getTelephone());
        assertEquals(userDto.getDateCreated(), userSaved.getDateCreated());
    }

    @Test
    void shouldFindUserByIdAndReturnSuccessful() {
        final var id = 12L;
        final var name = "Daniel";
        final var user = buildUser(id, name);

        when(userDao.findById(user.getId()))
                .thenReturn(java.util.Optional.of(user));

        final var idForSearch = 12L;
        final var byId = userService.findById(idForSearch);

        assertNotNull(byId);
    }

    @Test
    void shouldFindUserByIdAndReturnUserNotFoundException() {
        final var id = 12L;
        final var name = "Daniel";
        final var user = buildUser(id, name);

        when(userDao.findById(user.getId()))
                .thenReturn(java.util.Optional.of(user));

        final var idForSearch = 11L;

        final var userNotFoundException = Assertions.assertThrows(UserNotFoundException.class, () -> {
            userService.findById(idForSearch);
        });

        final var expectedMessage = "User not found";

        assertEquals(expectedMessage, userNotFoundException.getMessage());
    }

    @Test
    void shouldDeleteUserAndReturnEmpty() {
        final var id = 12L;
        final var name = "Daniel";
        final var user = buildUser(id, name);


        when(userDao.findById(id)).thenReturn(java.util.Optional.of(user));

        userService.delete(user.getId());

        verify(userDao, times(1)).delete(user);
    }

    @Test
    void shouldFindByDocumentAnPasswordAndReturnUser() {
        final var document = "174498418";
        final var password = "opt12!";

        when(userDao.findByDocumentAndPassword(document, password))
                .thenReturn(java.util.Optional.ofNullable(buildUser()));

        final var found = userService.findByDocumentAndPassword(document, password);

        assertNotNull(found);
    }

    @Test
    void shouldFindByDocumentAndPasswordAndReturnUserNotFound(){
        final var document = "174498418";
        final var password = "opt12!";

        when(userDao.findByDocumentAndPassword(document, password))
                .thenReturn(java.util.Optional.ofNullable(buildUser()));

        final var passwordWrong = "122";

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            userService.findByDocumentAndPassword(document, passwordWrong);
        });
    }

    @Test
    void shouldQueryByNameAndReturnListOfUser(){
        final var name1 = "Daniel";

        final var user1 = buildUser(1L, name1);

        when(userDao.queryByNameLike(name1))
                .thenReturn(List.of(user1));

        final var userDTOS = userService.queryByName(name1);

        assertFalse(userDTOS.isEmpty());
        assertEquals(1, userDTOS.size());
        assertEquals(name1, userDTOS.get(0).getName());
    }


}
