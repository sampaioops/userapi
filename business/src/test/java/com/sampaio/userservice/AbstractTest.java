package com.sampaio.userservice;

import com.sampaio.userservice.model.User;
import com.sampaio.userservice.repository.UserDao;
import com.sampaio.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;

@ContextConfiguration(classes = TestContextConfiguration.class)
@SpringBootTest
public abstract class AbstractTest {

    @MockBean
    protected UserDao userDao;

    @Autowired
    protected UserService userService;

    protected User buildUser() {
        return buildUser(15L, "Wilson");
    }

    protected User buildUser(final Long id, final String name) {
        return User.builder()
                .id(id)
                .name(name)
                .document("123")
                .email("test")
                .password("asas")
                .address("r")
                .telephone("222")
                .dateCreated(LocalDate.now())
                .build();
    }
}
