## 1.1.2 (2021-12-04)

No changes.

## 1.1.1 (2021-12-04)

No changes.

## 1.1.0 (2021-12-04)

### fix

- [Fix pipeline](sampaioops/userapi@a8fae1ad388b26c32793d3c4acdb39ba7cc2843b) by @sampaioops

## 1.0.9 (2021-05-31)

### Bug fixes

- [My new branch](sampaioops/userapi@4271b5aeca3122bbec23af3ccec7de8b742df01e) by @inv.dgoncalves

## 1.0.8 (2021-05-31)

### Features

- [Testing no MR](sampaioops/userapi@7d548247088330e6ef1786ba9b3b2938b10c5206) by @inv.dgoncalves

## 1.0.7 (2021-05-28)

### Features

- [My Feature](sampaioops/userapi@f38ebf81972c598c445c42085525740b35ecd1c6) by @inv.dgoncalves


### Bug fixes

- [My Bug fix 404](sampaioops/userapi@f6b151386bb58f12e993fbafc4371d40bb58116a) by @inv.dgoncalves

## 1.0.6 (2021-05-28)

### Bug fixes

- [My Six branch](sampaioops/userapi@856e3edea078a870dbb33d59f9ba1bdca2df21fa) by @inv.dgoncalves

## 1.0.5 (2021-05-28)

### Features

- [My five branch](sampaioops/userapi@0687e5487c4a9218e25118bd6a7204b52bdabd34) by @inv.dgoncalves

## 1.0.4 (2021-05-28)

No changes.

## 1.0.3 (2021-05-28)

### Features

- [My Third Branch](sampaioops/userapi@806b76098716a09a5f08cbe57c025e7d8a896d3a) by @sampaioops

## 1.0.2 (2021-05-28)

### Bug fixes

- [My Second Branch](sampaioops/userapi@b1be711d6eb0f80c9887061aa9d83d64f0a54da3) by @sampaioops

## 1.0.1 (2021-05-28)

### Features

- [My New branch](sampaioops/userapi@ba2634b8d2b3fbd2fd5b0daed5cf508415211d40) by @sampaioops
