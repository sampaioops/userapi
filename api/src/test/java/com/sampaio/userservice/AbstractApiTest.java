package com.sampaio.userservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sampaio.userservice.dto.UserDTO;
import com.sampaio.userservice.repository.UserRepository;
import com.sampaio.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

@ContextConfiguration(classes = TestContextConfiguration.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("testunit")
public abstract class AbstractApiTest {

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    protected UserService userService;

    @MockBean
    protected UserRepository userRepository;

    protected UserDTO buildUserDto(){
        return UserDTO.builder()
                .id(1L)
                .name("Daniel")
                .email("sampaio@gmail.com")
                .password("18181")
                .document("181811")
                .address("Rua")
                .telephone("229874")
                .dateCreated(LocalDate.now())
                .build();
    }


}
