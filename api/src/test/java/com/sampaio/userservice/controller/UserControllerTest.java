package com.sampaio.userservice.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sampaio.userservice.AbstractApiTest;
import com.sampaio.userservice.dto.UserDTO;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserControllerTest extends AbstractApiTest {

    private static final String PATH_USER = "/v1.0/users";

    @SneakyThrows
    @Test
    void shouldGetAllUsers() {
        final var userDTO = buildUserDto();
        given(userService.getAll()).willReturn(List.of(userDTO));

        MvcResult mvcResult = mockMvc.perform(get(PATH_USER)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final var jsonResult = mvcResult.getResponse().getContentAsString();

        final var usersList = objectMapper.readValue(jsonResult, new TypeReference<List<UserDTO>>() {
        });
        final var userSearch = usersList.stream().filter(u -> u.getId().equals(userDTO.getId())).findAny();

        assertTrue(userSearch.isPresent());
    }

    @SneakyThrows
    @Test
    void shouldFindById() {
        final var id = 1L;
        final var userDTO = buildUserDto();
        given(userService.findById(id)).willReturn(userDTO);

        final var mvcResult = mockMvc.perform(get(PATH_USER.concat("/").concat(String.valueOf(id)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final var jsonResult = mvcResult.getResponse().getContentAsString();

        final var userFound = objectMapper.readValue(jsonResult, UserDTO.class);

        assertNotNull(userFound);
    }

    @SneakyThrows
    @Test
    void shouldCreateUser() {
        final var userDTO = buildUserDto();
        given(userService.save(userDTO)).willReturn(userDTO);

        final var json = objectMapper.writeValueAsString(userDTO);

        mockMvc.perform(post(PATH_USER)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();
    }

    @SneakyThrows
    @Test
    void shouldDeleteUser() {
        final var userId = 1L;

        doNothing().when(userService).delete(Mockito.anyLong());

        mockMvc.perform(delete(format("%s/%s", PATH_USER, userId))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(userService, times(1)).delete(Mockito.anyLong());
    }

    @SneakyThrows
    @Test
    void shouldSearchByName(){
        final List<UserDTO> users = List.of(buildUserDto(), buildUserDto());

        when(userService.queryByName(anyString())).thenReturn(users);

        final  var queryParam = "search?name=";

        final var mvcResult = mockMvc.perform(get(format("%s/%s%s", PATH_USER, queryParam, anyString()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        final var jsonResult = mvcResult.getResponse().getContentAsString();

        final var usersFound = objectMapper.readValue(jsonResult, new TypeReference<List<UserDTO>>() {});

        final var userExpected = users.get(0);
        final var userActual = usersFound.get(0);

        verify(userService, times(1))
                .queryByName(anyString());

        assertEquals(users.size(), usersFound.size());
        assertEquals(userExpected.getId(), userActual.getId());
    }
}
